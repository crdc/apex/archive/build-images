# Apex Distribution Images

[WIP] None of these do anything yet, just here to test `mkosi`.

## Omni

### Build

Build all images.

```sh
mkdir mkosi.{cache,output}
make
```

Build a single image.

```sh
./build consul
```

### Setup

```sh
sudo systemd-nspawn -bi mkosi.output/apex-master.raw
/usr/bin/plantd-install
```

#### Database

The database password needs to be set manually for now.

```sh
su - postgresql
psql
\password postgres
# enter password x2
\q
exit
```

## Setup systemd-nspawn

This section is for configuring a Debian Stretch system for `systemd-nspawn`
containers used for distribution.

### Networking

Just use `NetworkManager`, `cockpit`, and `cockpit-networkmanager` on the host
system.

#### Attempt at systemd-networkd

All commands here are done as root, `sudo` has been omitted.

```sh
systemctl enable --now systemd-networkd systemd-resolved
systemctl disable networking NetworkManager
mv /etc/resolv.conf /etc/resolv.conf.old
ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf
```

Reboot, log back in and test that it works.

```sh
networkctl status lo

● 1: lo
       Link File: /usr/lib/systemd/network/99-default.link
    Network File: n/a
            Type: loopback
           State: carrier (unmanaged)
         Address: 127.0.0.1
                  ::1
```

Move the interface into `networkd` and create a bridge to use with the containers.

Create the bridge device.

```sh
cat > /etc/systemd/network/br0.netdev <<EOF
[NetDev]
Name=br0
Kind=bridge
EOF
```

Create the bridge network.

```sh
cat > /etc/systemd/network/br0.network <<EOF
[Match]
Name=br0

[Network]
Address=172.16.0.1/24
DHCPServer=yes
IPMasquerade=yes
EOF
```

The containers need to be using `systemd-{networkd,resolved}`, and whatever is
used to start them needs to specify a `--network-bridge=br0`. If a `.nspawn`
file is used it would contain a network section, along with anything else.

```sh
[Network]
Bridge=br0
```

## ToDo

* [x] provide IP address for containers
* [ ] use static IP addresses for containers
* [ ] add UEFI support
* [ ] run images as services
* [ ] create a single image for configure, master, and ui
* [ ] determine whether to run core/plant as a container or bare metal
