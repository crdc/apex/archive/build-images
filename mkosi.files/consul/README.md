# Consul

There's a couple of things that would need to be changed for production.

* remove `-dev` from `etc/defaults/consul`
* if no UI is required remove `-ui` from the same
* to control access to the agent change `0.0.0.0` to something better as well
