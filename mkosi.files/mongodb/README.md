# MongoDB

There's still a single step that needs to be performed after the image is
created. It would be nice if this were something that could be run as part of
the `mkosi.finalize` script but I may not understand the purpose of that file.

Start the image.

```sh
systemd-nspawn --network-bridge=br0 -bi mkosi.output/mongodb.raw
```

Run the ansible playbook.

```sh
ansible-playbook /etc/ansible/playbooks/mongodb/playbook.yml
```

Alternatively, it may be possible to do it without entering the image.

```sh
sudo cp mkosi.output/mongodb.raw /var/lib/machines/
sudo cp nspawn/mongodb.nspawn /etc/systemd/nspawn/
sudo machinectl start mongodb
sudo machinectl shell mongodb ansible-playbook /etc/ansible/playbooks/mongodb/playbook.yml
```

Make MongoDB listen to any IP.

```sh
sed -i 's/bindIp:\s.*$/bindIp: 0.0.0.0/' /etc/mongod.conf
```

## ToDo

* [ ] make mongodb accept connections not from localhost
* [ ] bind a location on the host for database storage
