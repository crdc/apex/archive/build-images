# Postgres

There's still a single step that needs to be performed after the image is
created. It would be nice if this were something that could be run as part of
the `mkosi.finalize` script but I may not understand the purpose of that file.

Start the image.

```sh
systemd-nspawn --network-bridge=br0 -bi mkosi.output/postgres.raw
```

Run the ansible playbook.

```sh
ansible-playbook /etc/ansible/playbooks/postgres/playbook.yml
```

Alternatively, it may be possible to do it without entering the image.

```sh
sudo cp mkosi.output/postgres.raw /var/lib/machines/
sudo cp nspawn/postgres.nspawn /etc/systemd/nspawn/
sudo machinectl start postgres
sudo machinectl shell postgres ansible-playbook /etc/ansible/playbooks/postgres/playbook.yml
```

## Setup Database

After all of this there was additional steps to get the database location to
have the configuration files, password, etc. This is just notes for now to
remind me of what needs to be fixed.

```sh
su - postgres
rm -rf 9.6/main
initdb --locale en_US.UTF-8 -E UTF8 -D '/var/lib/postgresql/9.6/main'
exit
systemctl restart postgresql
su - postgres
psql
\password
# enter password x2
\quit
exit
sed -i "s/^#\(listen_addresses\s=\s\).*$/\1'*'/" /etc/postgresql/9.6/main/postgresql.conf
cat >> /etc/postgresql/9.6/main/pg_hba.conf <<EOF
host    all             all             0.0.0.0/0                md5
host    all             all             ::/0                     md5
EOF
systemctl restart postgresql
```

## ToDo

* [x] make postgres accept connections not from localhost
* [ ] bind a location on the host for database storage
