M = $(shell printf "\033[34;1m▶\033[0m")

all: build

build: consul mongodb postgres ; $(info $(M) Building images...)
	@./build master

consul: ; $(info $(M) Building consul...)
	@./build consul

mongodb: ; $(info $(M) Building mongodb...)
	@./build mongodb

postgres: ; $(info $(M) Building postgres...)
	@./build postgres

clean: ; $(info $(M) [TODO] Removing generated files... )
	@rm -rf output

.PHONY: all build
